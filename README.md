## Apache Cordova with ionic and crud database
Requires:
* Node.js
* Ionic 4
* Angular 7
* Express and MongoDB API

Install Node.js from https://nodejs.org/   
Install MongoDB from https://docs.mongodb.com/manual/administration/install-community/   
Follow instructions https://www.djamware.com/post/58a91cdf80aca748640ce353/how-to-create-rest-api-easily-using-nodejs-expressjs-mongoosejs-and-mongodb  
```
npm --version
npm install -g ionic
npm install --save-dev @ionic/lab
npm install -g @angular/cli
ionic serve -l
```

Run
```
ionic serve -l
```





Ionic 4: 
<ionic-tab> deprecated

https://angular.io/   
https://cordova.apache.org/   
https://ionicframework.com/   